import java.util.Arrays;

public class QueueNormal {
    private long[] queueArray;
    private int nItems;
    private int front;
    private int rear;
    private int maxSize;

    QueueNormal(int s) {
        maxSize = s;
        queueArray = new long[s];
        front = 0;
        rear = -1;
        nItems = 0;
    }

    public void insertQueue(long value) {
        if (rear == maxSize-1) {
            rear = -1;
        }
        queueArray[++rear] = value;
        nItems++;
    }

    public long removeQueue() {
        long temp = queueArray[front++];
        if (front == maxSize) {
            front = 0;  
        }
        nItems--;
        return temp;
    }

    public int getSize() {
        return nItems;
    }

    public boolean isEmpty() {
        return nItems == 0;
    }

    public boolean isFull() {
        return nItems == maxSize;
    }

    @Override
    public String toString() {
        return "QueueNormal [queueArray=" + Arrays.toString(queueArray) + ", nItems=" + nItems + ", front=" + front
                + ", rear=" + rear + ", maxSize=" + maxSize + "]";
    }
}
