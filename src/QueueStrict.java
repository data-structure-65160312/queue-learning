import java.util.Arrays;

public class QueueStrict {
    private long[] queueArray;
    private int front;
    private int rear;
    private int maxSize;

    QueueStrict(int s) {
        maxSize = s;
        queueArray = new long[s];
        front = 0;
        rear = -1;
    }

    public void insertQueue(long value) {
        if (rear == maxSize - 1) {
            rear = -1;
        }
        queueArray[++rear] = value;
    }

    public long removeQueue() {
        long temp = queueArray[front++];
        if (front == maxSize) {
            front = 0;
        }
        return temp;
    }

    public long peek() {
        return queueArray[front];
    }

    public boolean isEmpty() {
        return rear + 1 == front || front + maxSize - 1 == rear;
    }

    public boolean isFull() {
        return rear + 2 == front || (front + maxSize - 2 == rear);
    }

    public int size() // (assumes queue not empty)
    {
        if (rear >= front) // contiguous sequence
            return rear - front + 1;
        else // broken sequence
            return (maxSize - front) + (rear + 1);
    }

}
